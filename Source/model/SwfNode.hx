package model;

import flash.display.DisplayObjectContainer;
import flash.events.Event;
import flash.events.EventDispatcher;
import flash.geom.Point;
import flash.geom.Rectangle;
import model.SwfNode;
import openfl.display.DisplayObject;

class SwfNode extends EventDispatcher {
    public var name:String;
    public var className:String;

    public var alpha:Float;
    public var visible:Bool;

    public var w:Float;
    public var h:Float;
    public var x:Float;
    public var y:Float;
    public var scaleX:Float;
    public var scaleY:Float;

    public var viewRect:Rectangle;

    public var parent:SwfNode;
    public var items:Array<SwfNode> = [];

    public var displayObject:DisplayObject;

    public function new(name:String) {
        super();
        this.name = name;
    }

    public var root(get, null):SwfNode;

    function get_root():SwfNode {
        var p = parent;
        var res = parent;
        while (p != null) {
            res = p;
            p = p.parent;
        }
        return res;
    }

    public function getFullPath():String {
        var res = name;
        var p = parent;
        while (p != null) {
            res = p.name + "/" + res;
            p = p.parent;
        }
        return res;
    }

    public function getDepth():Int {
        var res = 0;
        var p = parent;
        while (p != null) {
            res ++;
            p = p.parent;
        }
        return res;
    }

    public function getByDisplayObject(display:DisplayObject) {
        if (display == null) return null;
        return searchFirst(function (it:SwfNode) return it.displayObject == display);
    }

    public function searchFirst(match:SwfNode -> Bool):SwfNode {
        if (match(this))
            return this;

        for (i in items) {
            var res = i.searchFirst(match);
            if (res != null) return res;
        }
        return null;
    }

    public function search(match:SwfNode -> Bool, result:Array<SwfNode>) {
        if (match(this))
            result.push(this);

        for (i in items) {
            i.search(match, result);
        }
    }

    public function iter(it:SwfNode -> Void) {
        it(this);
        for (i in items) i.iter(it);
    }

    public function iterUp(it:SwfNode -> Void, self = true) {
        var p:SwfNode = self ? this : parent;
        while (p != null) {
            it(p);
            p = p.parent;
        }
    }

    public function setFlatVisible(value:Bool) {
        iter(function(it:SwfNode) {
            if (it.displayObject != null) {
                it.displayObject.visible = value;
                it.invalidate();
            }
        });

        if (value) {
            iterUp(function(it:SwfNode) {
                if (it.displayObject != null) {
                    it.displayObject.visible = value;
                    it.invalidate();
                }
            });
        }
    }

    public function getFlatVisible():Bool {
        var res = if (displayObject == null) false; else displayObject.visible;
        if (parent == null) return res;

        return res && parent.getFlatVisible();
    }

    public override function toString() return name;

    static public function fromSwf(node:DisplayObject):SwfNode {
        return _fromSwf(node, null, node);
    }

    static function _fromSwf(node:DisplayObject, parent:SwfNode, root:DisplayObject):SwfNode {
        if (node == null)
            return null;

        var res = new SwfNode(node.name);
        res.displayObject = node;
        res.alpha = node.alpha;
        res.visible = node.visible;
        res.w = node.width;
        res.h = node.height;
        var pos = node.localToGlobal(new Point());
        pos = root.globalToLocal(pos);
        res.x = pos.x;
        res.y = pos.y;
        res.scaleX = node.scaleX;
        res.scaleY = node.scaleY;
        res.className = Type.getClassName(Type.getClass(node));
        res.parent = parent;

        res.viewRect = node.getBounds(root);

        var cont:DisplayObjectContainer = Std.instance(node, DisplayObjectContainer);
        if (cont != null) {
            for (i in 0...cont.numChildren) {
                var child = _fromSwf(cont.getChildAt(i), res, root);
                if (child != null) res.items.push(child);
            }
        }
        return res;
    }

    public function invalidate():Void {
        dispatchEvent(new Event(Event.CHANGE));
    }

    public var sortID:Int;

    // tree view data
    public var expanded:Bool = false;
    public var index:Int;
    public var indent:Int;
}
