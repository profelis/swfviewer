package model;

import flash.display.Sprite;
import flash.display.Loader;
import flash.events.Event;
import flash.net.FileReference;
import flash.system.ApplicationDomain;
import flash.system.LoaderContext;
import haxe.Timer;
import mmvc.impl.Actor;
import msignal.Signal.Signal0;
import msignal.Signal.Signal1;
import msignal.Signal.Signal2;
import openfl.display.DisplayObject;

class SwfModel extends Actor {

    var loader:Loader = new Loader();

    public var fileName:String;

    public var focusNode:Signal1<SwfNode> = new Signal1<SwfNode>();
    public var selectNode:Signal1<SwfNode> = new Signal1<SwfNode>();
    public var data(default, null):SwfNode;
    public var focusedNode(default, null):SwfNode;
    public var selectedNode(default, null):SwfNode;
    public var content(default, null):DisplayObject;

    public var contentUpdated:Signal0 = new Signal0();

    public function new() {
        super();
        //loader.addEventListener(Event.INIT, emitUpdateAndParse);
        //loader.addEventListener(Event.COMPLETE, emitUpdateAndParse);
        loader.addEventListener(Event.UNLOAD, emitUpdate);

        var cont = new Sprite();
        cont.addChild(loader);
        content = cont;

        focusNode.add(function (node:SwfNode) focusedNode = node);
        selectNode.add(function (node:SwfNode) selectedNode = node);
    }

    function emitUpdateAndParse(e:Event = null) {
        var parent = loader.parent;
        if (parent != null) parent.removeChild(loader);
        data = SwfNode.fromSwf(loader.getChildAt(0));
        if (parent != null) parent.addChild(loader);
        emitUpdate(e);
    }

    function emitUpdate(e:Event = null) {
        contentUpdated.dispatch();
    }

    public function load(ref:FileReference):Bool {
        try {
            loader.unloadAndStop();
            data = null;
            focusNode.dispatch(null);
            selectNode.dispatch(null);
            loader.x = 0;
            loader.y = 0;
            loader.scaleX = 1.0;
            loader.scaleY = 1.0;
        } catch (e:Dynamic) {
            trace(e);
            return false;
        }

        var domain = new LoaderContext(false, ApplicationDomain.currentDomain, null);
        domain.allowCodeImport = true;
        try {
            loader.loadBytes(ref.data, domain);
            fileName = ref.name;
            Timer.delay(emitUpdateAndParse.bind(null), 300);
            return true;
        } catch (e:Dynamic) {
            trace(e);
            return false;
        }
    }
}
