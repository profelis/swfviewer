package controller;

import flash.text.TextField;
import mmvc.base.MediatorBase;
import model.SwfModel;
import model.SwfNode;
import view.PropertiesPanel;

class PropertiesMediator extends MediatorBase<PropertiesPanel> {

    @inject public var swfModel:SwfModel;

    override public function onRegister():Void {
        super.onRegister();
        mediate(swfModel.selectNode.add(onSelectNode));
    }

    function onSelectNode(node:SwfNode) {
        view.title = "Properties";
        if (node == null) {
            view.nameLabel.text = "";
            view.sizeInfo.text = "";
            view.scaleInfo.text = "";

            view.visibilityInfo.text = "";
            view.alphaInfo.text = "";
        }
        else {
            view.nameLabel.text = node.name;
            view.scaleInfo.text = 'sx:${round(node.scaleX)} sy:${round(node.scaleY)}';
            view.sizeInfo.text = 'x:${round(node.x)} y:${round(node.y)} w:${round(node.w)} h:${round(node.h)}';

            updateTitle(node);

            view.visibilityInfo.text = node.visible ? "visible" : "hidden";
            view.alphaInfo.text = 'alpha: ${node.alpha}';
            view.boundsInfo.text = 'bb: (${round(node.viewRect.x)} ${round(node.viewRect.y)} ${round(node.viewRect.width)} ${round(node.viewRect.height)})';

            view.draw();
        }
    }

    function updateTitle(node:SwfNode) {
        view.title = 'Properties:  ${node.getFullPath()}  (${node.className})';
        if (node.displayObject != null) {
            if (Std.is(node.displayObject, TextField))
                view.title += '  "${Std.instance(node.displayObject, TextField).text}"';
        }
    }

    inline function round(value:Float):Float {
        return Math.round(value * 100) / 100;
    }
}
