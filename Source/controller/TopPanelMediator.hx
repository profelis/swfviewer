package controller;

import flash.events.Event;
import flash.events.IOErrorEvent;
import flash.events.MouseEvent;
import mmvc.base.MediatorBase;
import model.SwfModel;
import flash.net.FileFilter;
import flash.net.FileReference;
import view.TopPanel;

class TopPanelMediator extends MediatorBase<TopPanel> {

    @inject public var swfModel:SwfModel;

    override public function onRegister():Void {
        super.onRegister();
        view.load.addEventListener(MouseEvent.CLICK, onLoadClick);
    }

    function onLoadClick(e:MouseEvent) {
        var ref = new FileReference();
        try {
            ref.browse([new FileFilter("SWF file", "*.swf")]);
            ref.addEventListener(Event.SELECT, onBrowseComplete);
        } catch (e:Dynamic) {
            trace(e);
        }
    }

    function onBrowseComplete(e:Event) {
        var ref:FileReference = cast(e.currentTarget, FileReference);
        view.label.text = ref.name;
        ref.addEventListener(IOErrorEvent.IO_ERROR, onLoadError);
        ref.addEventListener(Event.COMPLETE, onSwfLoaded);
        try {
            ref.load();
        } catch (e:Dynamic) {
            trace(e);
        }
    }

    function onSwfLoaded(e:Event) {
        var ref = cast(e.currentTarget, FileReference);
        if (!swfModel.load(ref)) onLoadError(e);
    }

    function onLoadError(e:Event) view.label.text = "try again";

    override public function onRemove():Void {
        super.onRemove();
        view.load.removeEventListener(MouseEvent.CLICK, onLoadClick);
    }
}
