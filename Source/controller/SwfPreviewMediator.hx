package controller;

import lime.ui.MouseCursor;
import lime.ui.Mouse;
import flash.ui.Keyboard;
import Reflect;
import haxe.ds.ArraySort;
import flash.events.KeyboardEvent;
import flash.events.MouseEvent;
import flash.geom.Point;
import flash.Lib;
import flash.display.Shape;
import flash.display.Graphics;
import mmvc.base.MediatorBase;
import model.SwfModel;
import model.SwfNode;
import view.SwfPreview;

class SwfPreviewMediator extends MediatorBase<SwfPreview> {

    @inject public var swfModel:SwfModel;
    var itemsUnderCursor:Array<SwfNode> = [];
    var itemUndexCursorIndex = 0;

    var dragMode:Int;
    var dragStart:Point;

    override public function onRegister():Void {
        super.onRegister();

        view.addChild(swfModel.content);
        view.addChild(view.selectionItemRect);
        view.addChild(view.focusItemRect);
        mediate(swfModel.contentUpdated.add(onContentUpdated));
        mediate(swfModel.selectNode.add(onSelectNode));
        mediate(swfModel.focusNode.add(onFocusNode));

        view.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
        view.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
        view.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
        Lib.current.stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
        Lib.current.stage.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
    }

    function onFocusNode(node:SwfNode) renderBounds(view.focusItemRect.graphics, node, 0, true, true);

    function onSelectNode(node:SwfNode) {
        view.focusItemRect.graphics.clear();
        renderBounds(view.selectionItemRect.graphics, node, 2);
    }

    function renderBounds(gfx:Graphics, node:SwfNode, lineWidth:Float, clear:Bool = true, fill = false) {
        if (clear) gfx.clear();
        if (node != null) {
            gfx.lineStyle(lineWidth, 0x00FF00, 0.5);
            var pos:Point = swfModel.content.localToGlobal(new Point(node.x, node.y));
            pos = view.selectionItemRect.globalToLocal(pos);
            gfx.drawRect(pos.x, pos.y, node.w, node.h);
            gfx.endFill();

            var rect = node.viewRect;
            pos = swfModel.content.localToGlobal(new Point(rect.x, rect.y));
            pos = view.selectionItemRect.globalToLocal(pos);
            gfx.lineStyle(lineWidth, 0xFF0000, 0.5);
            if (fill) gfx.beginFill(0x0000FF, 0.2);
            gfx.drawRect(pos.x, pos.y, rect.width, rect.height);
            gfx.endFill();
        }
    }

    function onContentUpdated() {
        itemUndexCursorIndex = 0;
        itemsUnderCursor.splice(0, itemsUnderCursor.length);
        dragMode = 0;
        Mouse.cursor = MouseCursor.DEFAULT;
        view.update();
    }

    function onMouseMove(e:MouseEvent) {
        if (dragMode == 2)
        {
            var mousePos = new Point(Lib.current.stage.mouseX, Lib.current.stage.mouseY);
            var delta = dragStart.subtract(mousePos);
            dragStart = mousePos;
            view.scrollCont(delta);
            return;
        }
        if (swfModel.data == null) return;

        var d = swfModel.data.displayObject;
        var pos = new Point(d.mouseX, d.mouseY);
        pos = d.localToGlobal(pos);
        //trace(pos);
        itemsUnderCursor.splice(0, itemsUnderCursor.length);
        swfModel.data.search(function (it:SwfNode) {
            if (it.displayObject == null) return false;
            return it.displayObject.hitTestPoint(pos.x, pos.y, true);
        }, itemsUnderCursor);

        var i = 0;
        while (i < itemsUnderCursor.length) {
            var item = itemsUnderCursor[i];
            if (item.parent != null && item.parent.items.length == 1 && Std.is(item.displayObject, Shape))
            {
                var found = false;
                for (it in itemsUnderCursor) if (it == item.parent) {
                    found = true;
                    break;
                }
                if (found) {
                    itemsUnderCursor.splice(i, 1);
                    continue;
                }
                itemsUnderCursor[i] = item.parent;
            }
            i++;
        }

        var toRemove = new Array<SwfNode>();
        for (i in itemsUnderCursor)
        {
            for (it in itemsUnderCursor) if (it.parent == i) {
                toRemove.push(i);
                break;
            }
        }
        for (i in toRemove) itemsUnderCursor.remove(i);

        ArraySort.sort(itemsUnderCursor, function (a:SwfNode, b:SwfNode) {
            var res = Reflect.compare(b.getDepth(), a.getDepth());
            if (res == 0) {
                if (a.parent != null && a.parent == b.parent)
                    return Reflect.compare(a.parent.items.indexOf(a), a.parent.items.indexOf(b));
            }
            return res;
        });
        //for (it in itemsUnderCursor) trace(it.getFullPath() + " " + it.getDepth());
        var nodeIndex = 0;
        var node:SwfNode = itemsUnderCursor[nodeIndex];
        if (!e.altKey) {
            node = null;
            for (i in 0...itemsUnderCursor.length) {
                var item = itemsUnderCursor[i];
                if (item.getFlatVisible()) {
                    node = item;
                    break;
                }
            }
        }
        if (node != swfModel.focusedNode) {
            swfModel.focusNode.dispatch(node);
            itemUndexCursorIndex = nodeIndex;
        }
    }

    function onMouseDown(e:MouseEvent) {
        if (dragMode == 1) {
            dragMode = 2;
            dragStart = new Point(Lib.current.stage.mouseX, Lib.current.stage.mouseY);
        }
    }

    function onMouseUp(e:MouseEvent) {
        if (dragMode == 2) {
            dragMode = 1;
            return;
        }
        if (swfModel.data == null) return;
        if (itemsUnderCursor.length == 0) return;
        if (itemUndexCursorIndex > itemsUnderCursor.length - 1) itemUndexCursorIndex = 0;
        var node = itemsUnderCursor[itemUndexCursorIndex];
        if (!e.altKey) {
            node = null;
            for (i in itemUndexCursorIndex...itemsUnderCursor.length) {
                var item = itemsUnderCursor[i];
                if (item.getFlatVisible()) {
                    node = item;
                    itemUndexCursorIndex = i;
                    break;
                }
            }
            if (node == null) {
                itemUndexCursorIndex = 0;
                for (i in itemUndexCursorIndex...itemsUnderCursor.length) {
                    var item = itemsUnderCursor[i];
                    if (item.getFlatVisible()) {
                        node = item;
                        itemUndexCursorIndex = i;
                        break;
                    }
                }
            }
        }
        swfModel.selectNode.dispatch(node);
        if (e.altKey) {
            swfModel.data.setFlatVisible(false);
            node.setFlatVisible(true);
        }
        itemUndexCursorIndex++;
    }

    function onKeyDown(e:KeyboardEvent) {
        if (e.keyCode == Keyboard.SPACE && view.hitTestPoint(view.mouseX, view.mouseY)) {
            if (dragMode == 0) dragMode = 1;
            Mouse.cursor = MouseCursor.MOVE;
        }
    }

    function onKeyUp(e:KeyboardEvent) {
        if (e.keyCode == Keyboard.SPACE) {
            dragMode = 0;
            Mouse.cursor = MouseCursor.DEFAULT;
        }
    }

    override public function onRemove():Void {
        super.onRemove();
        view.removeChild(swfModel.content);

        view.removeEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
        view.removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
        view.removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);
        Lib.current.stage.removeEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
        Lib.current.stage.removeEventListener(KeyboardEvent.KEY_UP, onKeyUp);
    }
}
