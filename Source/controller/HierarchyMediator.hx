package controller;

import flash.events.Event;
import flash.events.MouseEvent;
import haxe.ds.ArraySort;
import hx.strings.Strings;
import mmvc.base.MediatorBase;
import model.SwfModel;
import model.SwfNode;
import openfl.text.TextField;
import view.hierarchy.HierarchyItem;
import view.HierarchyPanel;
import flash.Lib;

class HierarchyMediator extends MediatorBase<HierarchyPanel> {

    @inject public var swfModel:SwfModel;
    var resizeMode:Bool = false;

    override public function onRegister():Void {
        super.onRegister();

        mediate(swfModel.contentUpdated.add(onModelChanged));
        mediate(swfModel.selectNode.add(onModelSelectNode));
        mediate(HierarchyItem.mouseOver.add(onItemMouseOver));
        view.tree.addEventListener(Event.SELECT, onSelectTreeNode);
        view.searchInput.addEventListener(Event.CHANGE, onSearchChange);
        view.clearSearch.addEventListener(MouseEvent.CLICK, onClearSearch);
        view.showAll.addEventListener(MouseEvent.CLICK, onShowAll);
        view.hideAll.addEventListener(MouseEvent.CLICK, onHideAll);
        view.expandAll.addEventListener(MouseEvent.CLICK, onExpandAll);
        view.collapseAll.addEventListener(MouseEvent.CLICK, onCollapseAll);

        view.resizeHolder.addEventListener(MouseEvent.MOUSE_DOWN, onResizeDown);
    }

    function onResizeDown(e:MouseEvent) {
        resizeMode = true;
        view.addEventListener(Event.ENTER_FRAME, onEnterFrame);
        flash.Lib.current.addEventListener(MouseEvent.MOUSE_UP, onResizeUp);
    }

    function onResizeUp(e:MouseEvent) {
        resizeMode = false;
        view.removeEventListener(Event.ENTER_FRAME, onEnterFrame);
        flash.Lib.current.removeEventListener(MouseEvent.MOUSE_UP, onResizeUp);
    }

    function onEnterFrame(e:Event) {
        if (resizeMode) {
            var w = view.mouseX;
            if (w < view.minWidth) w = view.minWidth;
            view.width = w;
        }
    }

    function onExpandAll(e:MouseEvent) view.tree.expandAll();

    function onCollapseAll(e:MouseEvent) view.tree.collapseAll();

    function onShowAll(e:MouseEvent = null) showAll(true);

    function onHideAll(e:MouseEvent = null) showAll(false);

    function showAll(visible:Bool) {
        var items = view.tree.items;
        for (i in items) {
            var s:SwfNode = Std.instance(i, SwfNode);
            if (s != null) s.setFlatVisible(visible);
        }
    }

    function onModelSelectNode(node:SwfNode) {
        if (view.tree.selectedItem == node) return;
        if (view.tree.items.indexOf(node) == -1) {
            view.tree.expandAll();
            view.tree.draw();
        }
        if (view.tree.items.indexOf(node) == -1 && view.searchInput.text.length > 0) {
            onClearSearch(null);
        }
        view.tree.selectedItem = node;
    }

    function onSelectTreeNode(e:Event) swfModel.selectNode.dispatch(Std.instance(view.tree.selectedItem, SwfNode));

    function onItemMouseOver(node:SwfNode, over:Bool) if (over) swfModel.focusNode.dispatch(node);

    function onSearchChange(e:Event) updateView(true);

    function onClearSearch(e:MouseEvent) {
        view.searchInput.text = "";
        updateView(true);
    }

    function onModelChanged() {
        view.searchInput.text = "";
        updateView();
    }

    function updateView(restoreSelection = false) {
        var selected = view.tree.selectedItem;

        if (swfModel.data == null) {
            view.searchInput.text = "";
            view.tree.items = [];
            return;
        }

        searchText = Strings.trim(view.searchInput.text);
        var searchTextLength = searchText.length;
        if (searchTextLength > 0) {
            var items:Array<SwfNode> = [];

            swfModel.data.iter(function(it:SwfNode) {
                it.displayObject.visible = false;
                it.sortID = getDistance(it);
                items.push(it);
            });

            var max = 0;
            for (i in items) if (i.sortID > max) max = i.sortID;
            max = Math.floor(0.5 * max);
            items = items.filter(function(it:SwfNode) return it.sortID >= max);
            ArraySort.sort(items, function(a:SwfNode, b:SwfNode) return a.sortID == b.sortID ? 0 : a.sortID < b.sortID ? 1 : -1);
            for (i in items) i.setFlatVisible(true);

            view.tree.items = items;
        }
        else {
            if (view.tree.items.length != 1 || view.tree.items[0] != swfModel.data)
                view.tree.items = [swfModel.data];
        }

        if (restoreSelection && selected != null) {
            view.tree.expandAll();
            view.tree.selectedItem = selected;
        }
    }

    var searchText:String;

    function getDistance(it:SwfNode):Int {
        var res = 0;
        inline function search(str:String, scale = 1) {
            if (Strings.contains(str, searchText)) res += searchText.length * 3 * scale;
            res += Strings.getFuzzyDistance(str, searchText) * scale;
        }

        search(it.name);
        search(it.className);

        var tf:TextField = Std.instance(it.displayObject, TextField);
        if (tf != null) search(Strings.trim(tf.text), 2);

        if (false) trace('$res ${it.name} ${it.className} $searchText');
        return res;
    }

    override public function onRemove():Void {
        super.onRemove();

        view.tree.removeEventListener(Event.SELECT, onSelectTreeNode);
        view.searchInput.removeEventListener(Event.CHANGE, onSearchChange);
        view.showAll.removeEventListener(MouseEvent.CLICK, onShowAll);
        view.hideAll.removeEventListener(MouseEvent.CLICK, onHideAll);
        view.expandAll.removeEventListener(MouseEvent.CLICK, onExpandAll);
        view.collapseAll.removeEventListener(MouseEvent.CLICK, onCollapseAll);

        view.resizeHolder.removeEventListener(MouseEvent.MOUSE_DOWN, onResizeDown);
        view.removeEventListener(Event.ENTER_FRAME, onEnterFrame);
        flash.Lib.current.removeEventListener(MouseEvent.MOUSE_UP, onResizeUp);
    }
}
