package;

import controller.HierarchyMediator;
import controller.PropertiesMediator;
import controller.SwfPreviewMediator;
import controller.TopPanelMediator;
import flash.display.Sprite;
import flash.events.Event;
import minimalcomps.components.Component;
import minimalcomps.components.Style;
import mmvc.api.IViewContainer;
import model.SwfModel;
import view.HierarchyPanel;
import view.MainView;
import view.PropertiesPanel;
import view.SwfPreview;
import view.TopPanel;


class Main extends Sprite {

    #if as3
    static function main() new Main();
    #end

    public function new() {

        super();
        Style.setStyle(Style.DARK);
        Component.initStage(stage);

        var main = new MainView();
        addChild(main);
        var app = new ApplicationContext(main);

        var onResize = function(e:Event = null) {
            main.width = stage.stageWidth;
            main.height = stage.stageHeight;
        };
        stage.addEventListener(Event.RESIZE, onResize);
        onResize();

        main.start();
    }
}

class ApplicationContext extends mmvc.impl.Context {
    public function new(contextView:IViewContainer = null, autoStartup:Bool = true) {
        super(contextView, autoStartup);
    }

    override public function startup():Void {
        injector.mapSingleton(SwfModel);

        mediatorMap.mapView(TopPanel, TopPanelMediator);
        mediatorMap.mapView(SwfPreview, SwfPreviewMediator);
        mediatorMap.mapView(HierarchyPanel, HierarchyMediator);
        mediatorMap.mapView(PropertiesPanel, PropertiesMediator);
    }
}