package view;

import minimalcomps.components.HBox;
import minimalcomps.components.Label;
import minimalcomps.components.PushButton;

class TopPanel extends HBox {

    public var load:PushButton;
    public var label:Label;

    override private function init():Void {
        super.init();

        load = new PushButton(this);
        load.label = "load";

        label = new Label(this);
        label.text = "Load file";
    }
}