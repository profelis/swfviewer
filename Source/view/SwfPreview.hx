package view;

import flash.display.Sprite;
import flash.events.Event;
import flash.geom.Point;
import minimalcomps.components.ScrollPane;

class SwfPreview extends ScrollPane {

    public var focusItemRect:Sprite;
    public var selectionItemRect:Sprite;

    override private function init():Void {
        super.init();
        dragContent = true;

        selectionItemRect = new Sprite();
        focusItemRect = new Sprite();
    }

    public function scrollCont(delta:Point):Void {
        if (delta.x != 0) {
            _hScrollbar.value += delta.x;
            _hScrollbar.dispatchEvent(new Event(Event.CHANGE));
        }
        if (delta.y != 0) {
            _vScrollbar.value += delta.y;
            _vScrollbar.dispatchEvent(new Event(Event.CHANGE));
        }
    }
}
