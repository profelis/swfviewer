package view;

import minimalcomps.components.Label;
import minimalcomps.components.HBox;
import minimalcomps.components.PushButton;
import com.bit101.components.TreeList;
import minimalcomps.components.InputText;
import minimalcomps.components.Window;
import view.hierarchy.HierarchyItem;

class HierarchyPanel extends Window {

    public var minWidth = 200;
    public var search:HBox;
    public var searchInput:InputText;
    public var clearSearch:PushButton;

    public var tools:HBox;
    public var showAll:PushButton;
    public var hideAll:PushButton;
    public var expandAll:PushButton;
    public var collapseAll:PushButton;

    public var tree:TreeList;

    public var resizeHolder:PushButton;

    override private function init():Void {
        super.init();

        width = minWidth;
        height = 500;
        draggable = false;
        hasMinimizeButton = false;
        title = "Hierarchy";

        // SEARCH

        search = new HBox(_panel);
        search.spacing = 3;

        new Label(search, 0, 0, "Search:");

        searchInput = new InputText(search);

        clearSearch = new PushButton(search);
        clearSearch.label = "clear";
        clearSearch.width = 50;

        search.setSize(width, Math.max(searchInput.height, clearSearch.height));
        searchInput.width = search.width - clearSearch.width - 50;

        // TOOLS

        tools = new HBox(_panel);
        showAll = new PushButton(tools, 0, 0, "Show all");
        hideAll = new PushButton(tools, 0, 0, "Hide all");
        expandAll = new PushButton(tools, 0, 0, "+");
        collapseAll = new PushButton(tools, 0, 0, "-");
        showAll.width = 60;
        hideAll.width = 60;
        expandAll.width = 30;
        collapseAll.width = 30;
        tools.setSize(width, showAll.height);
        tools.y = search.height;

        tree = new TreeList(_panel);
        tree.alternateRows = false;
        tree.listItemClass = HierarchyItem;

        resizeHolder = new PushButton();
        resizeHolder.label = "|";
        addRawChild(resizeHolder);
        resizeHolder.width = 10;
        resizeHolder.height = _titleBar.height;
    }

    override public function draw():Void {
        super.draw();

        search.width = _panel.width;

        tree.y = tools.y + tools.height;
        tree.setSize(_panel.width, _panel.height - search.height - tools.height);

        resizeHolder.height = _titleBar.height;
        resizeHolder.x = width - resizeHolder.width;
    }
}
