package view.hierarchy;

import flash.ui.Keyboard;
import flash.events.KeyboardEvent;
import com.bit101.components.TreeList;
import com.bit101.components.TreeListItem;
import flash.events.Event;
import flash.events.MouseEvent;
import minimalcomps.components.CheckBox;
import model.SwfNode;
import msignal.Signal.Signal2;

class HierarchyItem extends TreeListItem {

    static var inversedVisibility:String = null;
    static public var mouseOver:Signal2<SwfNode, Bool> = new Signal2<SwfNode, Bool>();

    var visibleButton:CheckBox;

    override function addChildren():Void {
        super.addChildren();

        addChild(visibleButton = new CheckBox());
        visibleButton.addEventListener(MouseEvent.CLICK, onVisibleClick);
        flash.Lib.current.stage.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
    }

    function onKeyUp(e:KeyboardEvent) {
        if (!_selected) return;
        if (e.keyCode == Keyboard.S) changeVisibility(e.altKey);
    }

    function onVisibleClick(e:MouseEvent) changeVisibility(e.altKey);

    function changeVisibility(alternative = false) {
        if (_data != null) {
            var node:SwfNode = Std.instance(_data, SwfNode);
            if (node != null) {
                if (alternative) {
                    var tree:TreeList = null;
                    if (parent != null && parent.parent != null && parent.parent.parent != null)
                        tree = Std.instance(parent.parent.parent.parent, TreeList);
                    var roots:Array<Dynamic> = tree != null ? tree.items : [node.root];
                    inline function showRoots(value:Bool) {
                        for (i in roots) {
                            var s:SwfNode = Std.instance(i, SwfNode);
                            if (s != null) s.setFlatVisible(value);
                        }
                    }
                    var fullPath = node.getFullPath();
                    if (inversedVisibility == null || inversedVisibility != fullPath) {
                        showRoots(false);
                        node.setFlatVisible(true);
                        inversedVisibility = fullPath;
                    }
                    else {
                        showRoots(true);
                        inversedVisibility = null;
                    }
                }
                else {
                    node.setFlatVisible(!node.getFlatVisible());
                }
            }
        }
        invalidate();
    }

    public override function draw():Void {
        super.draw();

        visibleButton.y = Math.max(0, (height - visibleButton.height) * 0.5);
        visibleButton.x = width - visibleButton.width - visibleButton.y;
        var node:SwfNode = _data != null ? Std.instance(_data, SwfNode) : null;

        if (node != null) {
            visibleButton.visible = true;
            visibleButton.selected = node.getFlatVisible();
        }
        else {
            visibleButton.visible = false;
        }
    }

    override private function onMouseOver(event:MouseEvent):Void {
        super.onMouseOver(event);
        var node = Std.instance(_data, SwfNode);
        if (node != null) mouseOver.dispatch(node, true);
    }

    override private function onMouseOut(event:MouseEvent):Void {
        super.onMouseOut(event);
        var node = Std.instance(_data, SwfNode);
        if (node != null) mouseOver.dispatch(node, false);
    }

    public override function set_data(value:Dynamic):Dynamic {
        var prev:SwfNode = Std.instance(data, SwfNode);
        if (prev != null) prev.removeEventListener(Event.CHANGE, onItemChange);

        var res:Dynamic = super.data = value;

        var next:SwfNode = Std.instance(res, SwfNode);
        if (next != null) next.addEventListener(Event.CHANGE, onItemChange);
        return res;
    }

    function onItemChange(e:Event) {
        invalidate();
    }
}
