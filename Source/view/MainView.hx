package view;

import flash.display.DisplayObject;
import flash.events.Event;
import haxe.Timer;
import minimalcomps.components.Panel;
import mmvc.api.IViewContainer;

class MainView extends Panel implements IViewContainer {

    public var viewAdded:Dynamic -> Void;
    public var viewRemoved:Dynamic -> Void;

    var listenHierarhyResize:Bool = true;

    public function isAdded(view:Dynamic):Bool {
        var v = Std.instance(view, DisplayObject);
        return v != null && contains(v);
    }

    public function new() {
        super();
        addEventListener(Event.ADDED, function(e:Event) {
            if (viewAdded != null) viewAdded(e.target);
        });
        addEventListener(Event.REMOVED, function(e:Event) {
            if (viewRemoved != null) viewRemoved(e.target);
        });
    }

    var top:TopPanel;
    var swf:SwfPreview;
    var hierarchy:HierarchyPanel;
    var props:PropertiesPanel;

    public function start() {
        top = new TopPanel();
        addChild(top);
        top.height = 40;
        swf = new SwfPreview();
        addChild(swf);

        hierarchy = new HierarchyPanel();
        addChild(hierarchy);
        hierarchy.y = top.height + 10;
        hierarchy.x = 10;
        hierarchy.addEventListener(Event.RESIZE, onHierarhyResize);

        props = new PropertiesPanel();
        addChild(props);

        Timer.delay(invalidate, 200);
    }

    function onHierarhyResize(e:Event) {
        if (listenHierarhyResize) invalidate();
    }

    override public function draw():Void {
        super.draw();
        top.x = 0;
        top.y = 0;
        top.width = width;

        listenHierarhyResize = false;
        hierarchy.move(0, top.height);
        hierarchy.height = height - hierarchy.y;
        listenHierarhyResize = true;

        props.move(hierarchy.width, height - props.height);
        props.width = width - hierarchy.width;

        swf.move(hierarchy.width, top.height);
        swf.setSize(width - hierarchy.width, height - swf.y - props.height);
    }
}
