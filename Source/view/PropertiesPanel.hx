package view;

import flash.text.TextFieldType;
import minimalcomps.components.InputText;
import haxe.Timer;
import minimalcomps.components.Label;
import minimalcomps.components.VBox;
import minimalcomps.components.Window;

class PropertiesPanel extends Window {

    var vboxes:Array<VBox> = [];

    public var nameLabel:InputText = new InputText();
    public var sizeInfo:Label = new Label();
    public var scaleInfo:Label = new Label();

    public var alphaInfo:Label = new Label();
    public var visibilityInfo:Label = new Label();
    public var boundsInfo:Label = new Label();

    public function new() {
        super();
        Timer.delay(invalidate, 200);
    }

    override private function init():Void {
        super.init();

        title = "Properties";
        draggable = false;
        hasMinimizeButton = false;
        height = 100;
        width = 500;

        var vbox = new VBox();
        vbox.setSize(250, _panel.height);
        vbox.spacing = 5;
        vboxes.push(vbox);
        _panel.addChild(vbox);
        vbox.addChild(nameLabel);
        vbox.addChild(scaleInfo);
        vbox.addChild(sizeInfo);
        nameLabel.width = 150;

        vbox = new VBox();
        vbox.setSize(250, _panel.height);
        vbox.spacing = 5;
        vboxes.push(vbox);
        _panel.addChild(vbox);
        vbox.addChild(visibilityInfo);
        vbox.addChild(alphaInfo);
        vbox.addChild(boundsInfo);
    }

    override public function draw():Void {
        super.draw();

        nameLabel.draw();
        scaleInfo.draw();
        sizeInfo.draw();

        visibilityInfo.draw();
        alphaInfo.draw();
        boundsInfo.draw();

        var x = 0.0;
        for (vbox in vboxes) {
            vbox.height = _panel.height;
            vbox.x = x;
            vbox.draw();
            x += vbox.width + 10;
        }
    }

}
