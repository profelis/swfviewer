/**
 * TreeList.as
 * Adam Harte
 * version 0.9.10
 * 
 * A scrolling list of selectable items, with expandable sub-items. 
 * 
 * Copyright (c) 2011 Keith Peters
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.bit101.components;

import flash.ui.Keyboard;
import flash.events.KeyboardEvent;
import minimalcomps.components.ListItem;
import flash.display.DisplayObjectContainer;
import flash.events.Event;
import minimalcomps.components.List;
import Reflect;

class TreeList extends minimalcomps.components.List {
    var _allItems:Array<Dynamic>;

    /**
		 * Constructor
		 * @param parent The parent DisplayDynamicContainer on which to add this TreeList.
		 * @param xpos The x position to place this component.
		 * @param ypos The y position to place this component.
		 * @param items A multi-dimentional array of items to display in the tree list. Dynamics with label and optional items array property.
		 */
    public function new(parent:DisplayObjectContainer = null, xpos:Float = 0, ypos:Float = 0, items:Array<Dynamic> = null) {
        if (items != null) {
            _allItems = items;
        }
        else {
            _allItems = [];
            _items = [];
        }

        super(parent, xpos, ypos, items);
    }

    /**
		 * Initilizes the component.
		 */
    override function init():Void {
        _listItemClass = TreeListItem;
        formatItems();
        updateItems();

        super.init();

        flash.Lib.current.stage.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
    }

    function onKeyUp(e:KeyboardEvent) {
        if (!hitTestPoint(mouseX, mouseY)) return;
        if (e.keyCode == Keyboard.LEFT) {
            // TODO: search root
            if (selectedItem != null && Reflect.hasField(selectedItem, "expanded")) {
                collapseItem(selectedItem);
            }
        } else if (e.keyCode == Keyboard.RIGHT) {
            if (selectedItem != null && Reflect.hasField(selectedItem, "expanded")) {
                expandItem(selectedItem);
            }
        } else if (e.keyCode == Keyboard.UP) {
            selectedIndex --;
        } else if (e.keyCode == Keyboard.DOWN) {
            selectedIndex ++;
        }else if (e.keyCode == Keyboard.PAGE_UP) {
            selectedIndex -= 10;
        }
        else if (e.keyCode == Keyboard.PAGE_DOWN) {
            selectedIndex += 10;
        }
    }

    function updateItems():Void {
        _items = new Array();
        updateBranch(_allItems);
        invalidate();
    }

    function updateBranch(value:Array<Dynamic>, startIndex:Int = 0):Int {
        var offset:Int = startIndex;
        for (i in 0...value.length) {
            _items.push(value[i]);

            if (Reflect.hasField(value[i], "expanded") && value[i].expanded) {
                if (hasItems(value[i])) {
                    offset = updateBranch(value[i].items, offset);
                }
            }
        }
        return offset;
    }

    /**
		 * Added extra fields to each items data Dynamic.
		 * @param	value
		 * @param	startIndex
		 * @param	indent
		 * @return
		 */
    function formatItems():Void {
        formatBranch(_allItems);
    }

    function formatBranch(value:Array<Dynamic>, startIndex:Int = 0, indent:Int = 0):Int {
        var offset:Int = startIndex;
        for (i in 0...value.length) {
            value[i].index = offset;
            value[i].indent = indent;
            if (!Reflect.hasField(value[i], 'expanded')) value[i].expanded = true;
            offset++;

            if (hasItems(value[i])) {
                offset = formatBranch(value[i].items, offset, indent + 1);
            }
        }
        return offset;
    }

    function expandCollapseBranch(value:Array<Dynamic>, startIndex:Int = 0, expand:Bool = true):Int {
        var offset:Int = startIndex;
        for (i in 0...value.length) {
            value[i].expanded = expand;
            offset++;

            if (hasItems(value[i])) {
                offset = expandCollapseBranch(value[i].items, offset, expand);
            }
        }
        return offset;
    }


    ///////////////////////////////////
    // public methods
    ///////////////////////////////////

    public function expandAll():Void {
        expandCollapseBranch(_allItems, 0, true);
        refresh();

        invalidate();
    }

    public function collapseAll():Void {
        expandCollapseBranch(_allItems, 0, false);
        refresh();

        invalidate();
    }

    public function expandItem(value:Dynamic):Void {
        value.expanded = true;
        refresh();

        invalidate();
    }

    public function collapseItem(value:Dynamic):Void {
        value.expanded = false;
        refresh();

        invalidate();
    }

    public function toggleExpandItem(value:Dynamic):Void {
        value.expanded = !value.expanded;
        refresh();

        invalidate();
    }


    ///////////////////////////////////
    // event handlers
    ///////////////////////////////////

    /**
		 * Called when a user selects an item in the list.
		 */
    override function onSelect(event:Event):Void {
        var item:TreeListItem = if (Std.is(event.target, TreeListItem)) cast(event.target, TreeListItem) else null;
        if (item == null) return;

        super.onSelect(event);

        if (hasItems(item.data)) {
            item.data.expanded = !item.data.expanded;

            refresh();

            if (stage != null) stage.focus = this;
        }
    }

    static public function hasItems(o:Dynamic) {
        if (o == null || !Reflect.hasField(o, "items"))
            return false;

        var items = Reflect.field(o, "items");
        return Std.is(items, Array) && cast(items, Array<Dynamic>).length > 0;
    }

    function refresh():Void {
        updateItems();

        var numItems:Int = Math.ceil(_height / _listItemHeight);
        numItems = Std.int(Math.min(numItems, _items.length));
        numItems = Std.int(Math.max(numItems, 1));
        if (_itemHolder.numChildren != numItems) {
            makeListItems();
        }

        fillItems();
        scrollToSelection();
    }

    ///////////////////////////////////
    // getter/setters
    ///////////////////////////////////

    /**
     * Sets / gets the list of items to be shown.
     */
    public override function set_items(value:Array<Dynamic>):Array<Dynamic> {
        _allItems = value;

        formatItems();
        updateItems();
        makeListItems();
        fillItems();

        invalidate();

        return value;
    }

    override private function onResize(event:Event):Void {
        for (i in 0 ... _itemHolder.numChildren) {
            cast(_itemHolder.getChildAt(i), TreeListItem).width = width;
        }
    }

    override private function scrollToSelection():Void {
        var numItems:Int = Math.ceil(_height / _listItemHeight);
        if (_selectedIndex != -1) {
            if (_scrollbar.value > _selectedIndex) {
                _scrollbar.value = _selectedIndex;
            }
            else if (_scrollbar.value + numItems < _selectedIndex) {
                _scrollbar.value = _selectedIndex - numItems + 1;
            }
        }
        else {
            _scrollbar.value = 0;
        }
        fillItems();
    }
}
